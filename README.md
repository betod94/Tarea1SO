# Tarea1SO


# Humberto Hernández Salinas

## [descripcion de la tarea](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/tarea-arch.md)

Hardware:
Processor: AMD A4-7210 APU with AMD Radeon R3 @ 1.80GHz (4 Cores), Motherboard: HP 8245 v001, Chipset: AMD Device 1566, Memory: 3584MB, Disk: 500GB TOSHIBA DT01ACA0, Graphics: AMD MULLINS (DRM 2.50.0 / 4.13.0-46-generic LLVM 5.0.0) 512MB, Audio: AMD Kabini HDMI/DP, Monitor: HP ALL-in-One, Network: Realtek RTL8111/8168/8411 + Realtek RTL8723BE PCIe Wireless

Software:
OS: Ubuntu 17.10, Kernel: 4.13.0-46-generic (x86_64), Desktop: GNOME Shell 3.26.2, Display Driver: radeon 7.9.0, OpenGL: 4.5 Mesa 17.2.8, File-System: ext4, Screen Resolution: 1600x900
