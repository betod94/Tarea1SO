
# lspci -v -t
## -v Es para dar información más detallada acerca de todos los dispositivos
## -t Es para mostrar un diagrama en forma de arbol que contiene todos los
##    buses, dispositivos y concecciones entre ellos.
-[0000:00]-+-00.0  Advanced Micro Devices, Inc. [AMD] Device 1566
           +-01.0  Advanced Micro Devices, Inc. [AMD/ATI] Mullins [Radeon R3 Graphics]
           +-01.1  Advanced Micro Devices, Inc. [AMD/ATI] Kabini HDMI/DP Audio
           +-02.0  Advanced Micro Devices, Inc. [AMD] Device 156b
           +-02.2-[01]----00.0  Realtek Semiconductor Co., Ltd. RTL8723BE PCIe Wireless Network Adapter
           +-02.3-[02]----00.0  Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
           +-08.0  Advanced Micro Devices, Inc. [AMD] Device 1537
           +-10.0  Advanced Micro Devices, Inc. [AMD] FCH USB XHCI Controller
           +-11.0  Advanced Micro Devices, Inc. [AMD] FCH SATA Controller [AHCI mode]
           +-12.0  Advanced Micro Devices, Inc. [AMD] FCH USB EHCI Controller
           +-13.0  Advanced Micro Devices, Inc. [AMD] FCH USB EHCI Controller
           +-14.0  Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller
           +-14.2  Advanced Micro Devices, Inc. [AMD] FCH Azalia Controller
           +-14.3  Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge
           +-18.0  Advanced Micro Devices, Inc. [AMD] Device 1580
           +-18.1  Advanced Micro Devices, Inc. [AMD] Device 1581
           +-18.2  Advanced Micro Devices, Inc. [AMD] Device 1582
           +-18.3  Advanced Micro Devices, Inc. [AMD] Device 1583
           +-18.4  Advanced Micro Devices, Inc. [AMD] Device 1584
           \-18.5  Advanced Micro Devices, Inc. [AMD] Device 1585

