# lsblk --all --fs --perms --paths

## --all lista los dispositivos vacios

## --fs muestra información acerca de los systemas de archivos

## --paths imprime completamente los caminos hacia los dispositivos

## --perms nos muestra información acerca del dueño del dispositivo


NAME FSTYPE LABEL UUID                                 MOUNTPOINT   SIZE OWNER GROUP MODE
/dev/loop0
                                                                         root  disk  brw-rw----
/dev/loop1
                                                                         root  disk  brw-rw----
/dev/loop2
                                                                         root  disk  brw-rw----
/dev/loop3
                                                                         root  disk  brw-rw----
/dev/loop4
                                                                         root  disk  brw-rw----
/dev/loop5
                                                                         root  disk  brw-rw----
/dev/loop6
                                                                         root  disk  brw-rw----
/dev/loop7
                                                                         root  disk  brw-rw----
/dev/sda
│                                                                 465.8G root  disk  brw-rw----
├─/dev/sda1
│    vfat         A64E-CD3D                            /boot/efi    260M root  disk  brw-rw----
├─/dev/sda2
│                                                                    16M root  disk  brw-rw----
├─/dev/sda3
│    ntfs   Windows
│                 3698351E9834DE55                                427.8G root  disk  brw-rw----
├─/dev/sda4
│    ntfs   Windows RE tools
│                 96C0749BC07482F3                                  980M root  disk  brw-rw----
├─/dev/sda5
│    ntfs   RECOVERY
│                 8A7C7BC47C7BAA19                                 17.2G root  disk  brw-rw----
└─/dev/sda6
     ext4         3f7d8727-c791-4e97-b742-c661df9bcff5 /           19.5G root  disk  brw-rw----
/dev/sdb
                                                                         root  disk  brw-rw----
/dev/sr0
                                                                   1024M root  cdrom brw-rw----

