# lshw -html

## -html Nos muestra el arbol del dispositivo como un html

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="generator"  content="lshw-unknown" />
<style type="text/css">
  .first {font-weight: bold; margin-left: none; padding-right: 1em;vertical-align: top; }
  .second {padding-left: 1em; width: 100%; vertical-align: center; }
  .id {font-family: monospace;}
  .indented {margin-left: 2em; border-left: dotted thin #dde; padding-bottom: 1em; }
  .node {border: solid thin #ffcc66; padding: 1em; background: #ffffcc; }
  .node-unclaimed {border: dotted thin #c3c3c3; padding: 1em; background: #fafafa; color: red; }
  .node-disabled {border: solid thin #f55; padding: 1em; background: #fee; color: gray; }
</style>
<title>centralsystem-20-c018la</title>
</head>
<body>
<div class="indented">
<table width="100%" class="node" summary="attributes of centralsystem-20-c018la">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">centralsystem-20-c018la</div></td></tr></thead>
 <tbody>
    <tr><td class="first">descripción: </td><td class="second">Todo en uno</td></tr>
    <tr><td class="first">producto: </td><td class="second">20-c018la (X5Z67AA#ABM)</td></tr>
    <tr><td class="first">fabricante: </td><td class="second">HP</td></tr>
    <tr><td class="first">serie: </td><td class="second">8CC6510TF5</td></tr>
    <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
    <tr><td class="first">capacidades: </td><td class="second"><dfn title="SMBIOS version 2.8">smbios-2.8</dfn> <dfn title="DMI version 2.8">dmi-2.8</dfn> <dfn title="Symmetric Multi-Processing">smp</dfn> <dfn title="procesos 32-bit">vsyscall32</dfn> </td></tr>
    <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de centralsystem-20-c018la"><tr><td class="sub-first"> boot</td><td>=</td><td>normal</td></tr><tr><td class="sub-first"> chassis</td><td>=</td><td>all-in-one</td></tr><tr><td class="sub-first"> family</td><td>=</td><td>103C_53316J HP Desktop</td></tr><tr><td class="sub-first"> sku</td><td>=</td><td>X5Z67AA#ABM</td></tr><tr><td class="sub-first"> uuid</td><td>=</td><td>7728D23C-AC20-C7E9-BE2D-CEF2AD4EA42B</td></tr></table></td></tr>
 </tbody></table></div>
<div class="indented">
        <div class="indented">
    <table width="100%" class="node" summary="attributes of core">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">core</div></td></tr></thead>
 <tbody>
       <tr><td class="first">descripción: </td><td class="second">Placa base</td></tr>
       <tr><td class="first">producto: </td><td class="second">8245</td></tr>
       <tr><td class="first">fabricante: </td><td class="second">HP</td></tr>
       <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
       <tr><td class="first">versión: </td><td class="second">001</td></tr>
       <tr><td class="first">serie: </td><td class="second">PGBNT0A8J570N9</td></tr>
 </tbody>    </table></div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of firmware">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">firmware</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">BIOS</td></tr>
          <tr><td class="first">fabricante: </td><td class="second">AMI</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
          <tr><td class="first">versión: </td><td class="second">F.20</td></tr>
          <tr><td class="first">date: </td><td class="second">10/11/2016</td></tr>
          <tr><td class="first">tamaño: </td><td class="second">64KiB</td></tr>
          <tr><td class="first">capacidad: </td><td class="second">8128KiB</td></tr>
          <tr><td class="first">capacidades: </td><td class="second"><dfn title="bus PCI">pci</dfn> <dfn title="Conectar y listo">pnp</dfn> <dfn title="EEPROM BIOS puede actualizarse">upgrade</dfn> <dfn title="BIOS aliasing">shadowing</dfn> <dfn title="Arranque desde CD-ROM/DVD">cdboot</dfn> <dfn title="Ruta de arranque seleccionable">bootselect</dfn> <dfn title="Extensiones de disco optimizadas">edd</dfn> <dfn title="Muestra la clave en pantalla">int5printscreen</dfn> <dfn title="Controlador de teclado i8042">int9keyboard</dfn> <dfn title="Controlador de puerto de serie INT14">int14serial</dfn> <dfn title="Controlador de impresora INT17">int17printer</dfn> <dfn title="ACPI">acpi</dfn> <dfn title="Emulacin heredable USB">usb</dfn> <dfn title="Especificacin de arranque de la BIOS">biosbootspecification</dfn> <dfn title="La tecla de funcin inici el arranque en red">netboot</dfn> <dfn title="UEFI specification is supported">uefi</dfn> </td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of memory">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">memory</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">Memoria de sistema</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">27</div></td></tr>
          <tr><td class="first">ranura: </td><td class="second">Placa de sistema o placa base</td></tr>
          <tr><td class="first">tamaño: </td><td class="second">4GiB</td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of bank">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">bank</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">SODIMM DDR3 Síncrono Unbuffered (Unregistered) 1600 MHz (0.6 ns)</td></tr>
             <tr><td class="first">producto: </td><td class="second">M471B5173EB0-YK0</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Samsung</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
             <tr><td class="first">serie: </td><td class="second">1763F034</td></tr>
             <tr><td class="first">ranura: </td><td class="second">DIMM 0</td></tr>
             <tr><td class="first">tamaño: </td><td class="second">4GiB</td></tr>
             <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">1600MHz (0.6ns)</td></tr>
 </tbody>          </table></div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">L1 caché</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">2b</div></td></tr>
          <tr><td class="first">ranura: </td><td class="second">L1 CACHE</td></tr>
          <tr><td class="first">tamaño: </td><td class="second">256KiB</td></tr>
          <tr><td class="first">capacidad: </td><td class="second">256KiB</td></tr>
          <tr><td class="first">reloj: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capacidades: </td><td class="second"><dfn title="Enrutado sobrepasado">pipeline-burst</dfn> <dfn title="Interno">internal</dfn> <dfn title="Escrito de nuevo">write-back</dfn> <dfn title="Memoria unificada">unified</dfn> </td></tr>
          <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de cache:0"><tr><td class="sub-first"> level</td><td>=</td><td>1</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cache:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cache:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">L2 caché</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">2c</div></td></tr>
          <tr><td class="first">ranura: </td><td class="second">L2 CACHE</td></tr>
          <tr><td class="first">tamaño: </td><td class="second">2MiB</td></tr>
          <tr><td class="first">capacidad: </td><td class="second">2MiB</td></tr>
          <tr><td class="first">reloj: </td><td class="second">1GHz (1.0ns)</td></tr>
          <tr><td class="first">capacidades: </td><td class="second"><dfn title="Enrutado sobrepasado">pipeline-burst</dfn> <dfn title="Interno">internal</dfn> <dfn title="Escrito de nuevo">write-back</dfn> <dfn title="Memoria unificada">unified</dfn> </td></tr>
          <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de cache:1"><tr><td class="sub-first"> level</td><td>=</td><td>2</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of cpu">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">cpu</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">CPU</td></tr>
          <tr><td class="first">producto: </td><td class="second">AMD A4-7210 APU with AMD Radeon R3 Graphics</td></tr>
          <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices [AMD]</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">2d</div></td></tr>
          <tr><td class="first">información del bus: </td><td class="second"><div class="id">cpu@0</div></td></tr>
          <tr><td class="first">versión: </td><td class="second">AMD A4-7210 APU with AMD Radeon R3 Graphics</td></tr>
          <tr><td class="first">ranura: </td><td class="second">P0</td></tr>
          <tr><td class="first">tamaño: </td><td class="second">1032MHz</td></tr>
          <tr><td class="first">capacidad: </td><td class="second">1800MHz</td></tr>
          <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
          <tr><td class="first">reloj: </td><td class="second">100MHz</td></tr>
          <tr><td class="first">capacidades: </td><td class="second"><dfn title="64bits extensions (x86-64)">x86-64</dfn> <dfn title="mathematical co-processor">fpu</dfn> <dfn title="FPU exceptions reporting">fpu_exception</dfn> <dfn title="">wp</dfn> <dfn title="virtual mode extensions">vme</dfn> <dfn title="debugging extensions">de</dfn> <dfn title="page size extensions">pse</dfn> <dfn title="time stamp counter">tsc</dfn> <dfn title="model-specific registers">msr</dfn> <dfn title="4GB+ memory addressing (Physical Address Extension)">pae</dfn> <dfn title="machine check exceptions">mce</dfn> <dfn title="compare and exchange 8-byte">cx8</dfn> <dfn title="on-chip advanced programmable interrupt controller (APIC)">apic</dfn> <dfn title="fast system calls">sep</dfn> <dfn title="memory type range registers">mtrr</dfn> <dfn title="page global enable">pge</dfn> <dfn title="machine check architecture">mca</dfn> <dfn title="conditional move instruction">cmov</dfn> <dfn title="page attribute table">pat</dfn> <dfn title="36-bit page size extensions">pse36</dfn> <dfn title="">clflush</dfn> <dfn title="multimedia extensions (MMX)">mmx</dfn> <dfn title="fast floating point save/restore">fxsr</dfn> <dfn title="streaming SIMD extensions (SSE)">sse</dfn> <dfn title="streaming SIMD extensions (SSE2)">sse2</dfn> <dfn title="HyperThreading">ht</dfn> <dfn title="fast system calls">syscall</dfn> <dfn title="no-execute bit (NX)">nx</dfn> <dfn title="multimedia extensions (MMXExt)">mmxext</dfn> <dfn title="">fxsr_opt</dfn> <dfn title="">pdpe1gb</dfn> <dfn title="">rdtscp</dfn> <dfn title="">constant_tsc</dfn> <dfn title="">rep_good</dfn> <dfn title="">acc_power</dfn> <dfn title="">nopl</dfn> <dfn title="">nonstop_tsc</dfn> <dfn title="">cpuid</dfn> <dfn title="">extd_apicid</dfn> <dfn title="">aperfmperf</dfn> <dfn title="">pni</dfn> <dfn title="">pclmulqdq</dfn> <dfn title="">monitor</dfn> <dfn title="">ssse3</dfn> <dfn title="">cx16</dfn> <dfn title="">sse4_1</dfn> <dfn title="">sse4_2</dfn> <dfn title="">movbe</dfn> <dfn title="">popcnt</dfn> <dfn title="">aes</dfn> <dfn title="">xsave</dfn> <dfn title="">avx</dfn> <dfn title="">f16c</dfn> <dfn title="">rdrand</dfn> <dfn title="">lahf_lm</dfn> <dfn title="">cmp_legacy</dfn> <dfn title="">svm</dfn> <dfn title="">extapic</dfn> <dfn title="">cr8_legacy</dfn> <dfn title="">abm</dfn> <dfn title="">sse4a</dfn> <dfn title="">misalignsse</dfn> <dfn title="">3dnowprefetch</dfn> <dfn title="">osvw</dfn> <dfn title="">ibs</dfn> <dfn title="">skinit</dfn> <dfn title="">wdt</dfn> <dfn title="">topoext</dfn> <dfn title="">perfctr_nb</dfn> <dfn title="">bpext</dfn> <dfn title="">ptsc</dfn> <dfn title="">perfctr_l2</dfn> <dfn title="">cpb</dfn> <dfn title="">hw_pstate</dfn> <dfn title="">retpoline</dfn> <dfn title="">retpoline_amd</dfn> <dfn title="">rsb_ctxsw</dfn> <dfn title="">ssbd</dfn> <dfn title="">ls_cfg_ssbd</dfn> <dfn title="">vmmcall</dfn> <dfn title="">bmi1</dfn> <dfn title="">xsaveopt</dfn> <dfn title="">arat</dfn> <dfn title="">npt</dfn> <dfn title="">lbrv</dfn> <dfn title="">svm_lock</dfn> <dfn title="">nrip_save</dfn> <dfn title="">tsc_scale</dfn> <dfn title="">flushbyasid</dfn> <dfn title="">decodeassists</dfn> <dfn title="">pausefilter</dfn> <dfn title="">pfthreshold</dfn> <dfn title="">overflow_recov</dfn> <dfn title="CPU Frequency scaling">cpufreq</dfn> </td></tr>
          <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de cpu"><tr><td class="sub-first"> cores</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> enabledcores</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> threads</td><td>=</td><td>4</td></tr></table></td></tr>
 </tbody>       </table></div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:0</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">producto: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">100</div></td></tr>
          <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:00.0</div></td></tr>
          <tr><td class="first">versión: </td><td class="second">00</td></tr>
          <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
          <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
 </tbody>       </table></div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of display">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">display</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">VGA compatible controller</td></tr>
             <tr><td class="first">producto: </td><td class="second">Mullins [Radeon R3 Graphics]</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:01.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">45</td></tr>
             <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">vga_controller</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="extension ROM">rom</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de display"><tr><td class="sub-first"> driver</td><td>=</td><td>radeon</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de display"><tr><td class="sub-first"> irq</td><td>:</td><td>37</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>c0000000-cfffffff</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>d0000000-d07fffff</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f000(size=256)</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea00000-fea3ffff</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>c0000-dffff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of multimedia:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">Audio device</td></tr>
             <tr><td class="first">producto: </td><td class="second">Kabini HDMI/DP Audio</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD/ATI]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">1.1</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:01.1</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">00</td></tr>
             <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de multimedia:0"><tr><td class="sub-first"> driver</td><td>=</td><td>snd_hda_intel</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de multimedia:0"><tr><td class="sub-first"> irq</td><td>:</td><td>42</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea64000-fea67fff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">producto: </td><td class="second">Family 16h Processor Functions 5:1</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">2.2</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:02.2</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">00</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de pci:0"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de pci:0"><tr><td class="sub-first"> irq</td><td>:</td><td>25</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>e000(size=4096)</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fe900000-fe9fffff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of network">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">network</div></td></tr></thead>
 <tbody>
                <tr><td class="first">descripción: </td><td class="second">Interfaz inalámbrica</td></tr>
                <tr><td class="first">producto: </td><td class="second">RTL8723BE PCIe Wireless Network Adapter</td></tr>
                <tr><td class="first">fabricante: </td><td class="second">Realtek Semiconductor Co., Ltd.</td></tr>
                <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:01:00.0</div></td></tr>
                <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">wlp1s0</div></td></tr>
                <tr><td class="first">versión: </td><td class="second">00</td></tr>
                <tr><td class="first">serie: </td><td class="second">3c:a0:67:68:1a:5f</td></tr>
                <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="">ethernet</dfn> <dfn title="Interfaz fsica">physical</dfn> <dfn title="LAN inalmbrica">wireless</dfn> </td></tr>
                <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de network"><tr><td class="sub-first"> broadcast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> driver</td><td>=</td><td>rtl8723be</td></tr><tr><td class="sub-first"> driverversion</td><td>=</td><td>4.13.0-46-generic</td></tr><tr><td class="sub-first"> firmware</td><td>=</td><td>N/A</td></tr><tr><td class="sub-first"> ip</td><td>=</td><td>192.168.1.91</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr><tr><td class="sub-first"> link</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> multicast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> wireless</td><td>=</td><td>IEEE 802.11</td></tr></table></td></tr>
                <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de network"><tr><td class="sub-first"> irq</td><td>:</td><td>40</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>e000(size=256)</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fe900000-fe903fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of pci:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">PCI bridge</td></tr>
             <tr><td class="first">producto: </td><td class="second">Family 16h Processor Functions 5:1</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">2.3</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:02.3</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">00</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="">pci</dfn> <dfn title="Power Management">pm</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="">normal_decode</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de pci:1"><tr><td class="sub-first"> driver</td><td>=</td><td>pcieport</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de pci:1"><tr><td class="sub-first"> irq</td><td>:</td><td>27</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d000(size=4096)</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fe800000-fe8fffff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of network">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">network</div></td></tr></thead>
 <tbody>
                <tr><td class="first">descripción: </td><td class="second">Ethernet interface</td></tr>
                <tr><td class="first">producto: </td><td class="second">RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller</td></tr>
                <tr><td class="first">fabricante: </td><td class="second">Realtek Semiconductor Co., Ltd.</td></tr>
                <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:02:00.0</div></td></tr>
                <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">enp2s0</div></td></tr>
                <tr><td class="first">versión: </td><td class="second">10</td></tr>
                <tr><td class="first">serie: </td><td class="second">c8:d3:ff:e0:e8:ca</td></tr>
                <tr><td class="first">tamaño: </td><td class="second">10Mbit/s</td></tr>
                <tr><td class="first">capacidad: </td><td class="second">1Gbit/s</td></tr>
                <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
                <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
                <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="Vital Product Data">vpd</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> <dfn title="">ethernet</dfn> <dfn title="Interfaz fsica">physical</dfn> <dfn title="par trenzado">tp</dfn> <dfn title="Media Independent Interface">mii</dfn> <dfn title="10Mbit/s">10bt</dfn> <dfn title="10Mbit/s (bidireccional)">10bt-fd</dfn> <dfn title="100Mbit/s">100bt</dfn> <dfn title="100Mbit/s  (bidireccional)">100bt-fd</dfn> <dfn title="1Gbit/s">1000bt</dfn> <dfn title="1Gbit/s (bidireccional)">1000bt-fd</dfn> <dfn title="Autonegociacin">autonegotiation</dfn> </td></tr>
                <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de network"><tr><td class="sub-first"> autonegotiation</td><td>=</td><td>on</td></tr><tr><td class="sub-first"> broadcast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> driver</td><td>=</td><td>r8169</td></tr><tr><td class="sub-first"> driverversion</td><td>=</td><td>2.3LK-NAPI</td></tr><tr><td class="sub-first"> duplex</td><td>=</td><td>half</td></tr><tr><td class="sub-first"> firmware</td><td>=</td><td>rtl8168g-3_0.0.1 04/23/13</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr><tr><td class="sub-first"> link</td><td>=</td><td>no</td></tr><tr><td class="sub-first"> multicast</td><td>=</td><td>yes</td></tr><tr><td class="sub-first"> port</td><td>=</td><td>MII</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>10Mbit/s</td></tr></table></td></tr>
                <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de network"><tr><td class="sub-first"> irq</td><td>:</td><td>35</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>d000(size=256)</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fe804000-fe804fff</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fe800000-fe803fff</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of generic">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">generic</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">Encryption controller</td></tr>
             <tr><td class="first">producto: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">8</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:08.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">00</td></tr>
             <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="MSI-X">msix</dfn> <dfn title="HyperTransport">ht</dfn> <dfn title="Power Management">pm</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de generic"><tr><td class="sub-first"> driver</td><td>=</td><td>ccp</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de generic"><tr><td class="sub-first"> irq</td><td>:</td><td>0</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>d0800000-d081ffff</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fe700000-fe7fffff</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea6f000-fea6ffff</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea6a000-fea6bfff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH USB XHCI Controller</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">10</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:10.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">11</td></tr>
             <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="MSI-X">msix</dfn> <dfn title="PCI Express">pciexpress</dfn> <dfn title="">xhci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>xhci_hcd</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de usb:0"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea68000-fea69fff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost:0</div></td></tr></thead>
 <tbody>
                <tr><td class="first">producto: </td><td class="second">xHCI Host Controller</td></tr>
                <tr><td class="first">fabricante: </td><td class="second">Linux 4.13.0-46-generic xhci-hcd</td></tr>
                <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
                <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@3</div></td></tr>
                <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">usb3</div></td></tr>
                <tr><td class="first">versión: </td><td class="second">4.13</td></tr>
                <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usbhost:0"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost:1</div></td></tr></thead>
 <tbody>
                <tr><td class="first">producto: </td><td class="second">xHCI Host Controller</td></tr>
                <tr><td class="first">fabricante: </td><td class="second">Linux 4.13.0-46-generic xhci-hcd</td></tr>
                <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@4</div></td></tr>
                <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">usb4</div></td></tr>
                <tr><td class="first">versión: </td><td class="second">4.13</td></tr>
                <tr><td class="first">capacidades: </td><td class="second"><dfn title="">usb-3.00</dfn> </td></tr>
                <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usbhost:1"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>5000Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of storage">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">storage</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">SATA controller</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH SATA Controller [AHCI mode]</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">11</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:11.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">39</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="">storage</dfn> <dfn title="Power Management">pm</dfn> <dfn title="Message Signalled Interrupts">msi</dfn> <dfn title="">ahci_1.0</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de storage"><tr><td class="sub-first"> driver</td><td>=</td><td>ahci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de storage"><tr><td class="sub-first"> irq</td><td>:</td><td>34</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f140(size=8)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f130(size=4)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f120(size=8)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f110(size=4)</td></tr><tr><td class="sub-first"> ioport</td><td>:</td><td>f100(size=16)</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea6e000-fea6e3ff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH USB EHCI Controller</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">12</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:12.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">39</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Debug port">debug</dfn> <dfn title="Enhanced Host Controller Interface (USB2)">ehci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>ehci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de usb:1"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea6d000-fea6d0ff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">producto: </td><td class="second">EHCI Host Controller</td></tr>
                <tr><td class="first">fabricante: </td><td class="second">Linux 4.13.0-46-generic ehci_hcd</td></tr>
                <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@1</div></td></tr>
                <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">usb1</div></td></tr>
                <tr><td class="first">versión: </td><td class="second">4.13</td></tr>
                <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usb">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">descripción: </td><td class="second">USB hub</td></tr>
                   <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc.</td></tr>
                   <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
                   <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@1:1</div></td></tr>
                   <tr><td class="first">versión: </td><td class="second">0.18</td></tr>
                   <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                   <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">descripción: </td><td class="second">Ratón</td></tr>
                      <tr><td class="first">producto: </td><td class="second">USB Optical Mouse</td></tr>
                      <tr><td class="first">fabricante: </td><td class="second">PixArt</td></tr>
                      <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
                      <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@1:1.1</div></td></tr>
                      <tr><td class="first">versión: </td><td class="second">1.00</td></tr>
                      <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                      <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>2Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">descripción: </td><td class="second">Teclado</td></tr>
                      <tr><td class="first">producto: </td><td class="second">USB Keyboard</td></tr>
                      <tr><td class="first">fabricante: </td><td class="second">NOVATEK</td></tr>
                      <tr><td class="first">id físico: </td><td class="second"><div class="id">2</div></td></tr>
                      <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@1:1.2</div></td></tr>
                      <tr><td class="first">versión: </td><td class="second">1.04</td></tr>
                      <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 1.1">usb-1.10</dfn> </td></tr>
                      <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>usbhid</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>2Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:2</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">descripción: </td><td class="second">Interfaz Bluetooth</td></tr>
                      <tr><td class="first">producto: </td><td class="second">Bluetooth Radio</td></tr>
                      <tr><td class="first">fabricante: </td><td class="second">Realtek</td></tr>
                      <tr><td class="first">id físico: </td><td class="second"><div class="id">3</div></td></tr>
                      <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@1:1.3</div></td></tr>
                      <tr><td class="first">versión: </td><td class="second">2.00</td></tr>
                      <tr><td class="first">serie: </td><td class="second">00e04c000001</td></tr>
                      <tr><td class="first">capacidades: </td><td class="second"><dfn title="Radio Bluetooth">bluetooth</dfn> <dfn title="">usb-2.10</dfn> </td></tr>
                      <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:2"><tr><td class="sub-first"> driver</td><td>=</td><td>btusb</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>500mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>12Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
                </div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of usb:2">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:2</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">USB controller</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH USB EHCI Controller</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">13</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:13.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">39</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="Debug port">debug</dfn> <dfn title="Enhanced Host Controller Interface (USB2)">ehci</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:2"><tr><td class="sub-first"> driver</td><td>=</td><td>ehci-pci</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de usb:2"><tr><td class="sub-first"> irq</td><td>:</td><td>18</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea6c000-fea6c0ff</td></tr></table></td></tr>
 </tbody>          </table></div>
<div class="indented">
                          <div class="indented">
             <table width="100%" class="node" summary="attributes of usbhost">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usbhost</div></td></tr></thead>
 <tbody>
                <tr><td class="first">producto: </td><td class="second">EHCI Host Controller</td></tr>
                <tr><td class="first">fabricante: </td><td class="second">Linux 4.13.0-46-generic ehci_hcd</td></tr>
                <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
                <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@2</div></td></tr>
                <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">usb2</div></td></tr>
                <tr><td class="first">versión: </td><td class="second">4.13</td></tr>
                <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usbhost"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>2</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>             </table></div>
<div class="indented">
                                <div class="indented">
                <table width="100%" class="node" summary="attributes of usb">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb</div></td></tr></thead>
 <tbody>
                   <tr><td class="first">descripción: </td><td class="second">USB hub</td></tr>
                   <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc.</td></tr>
                   <tr><td class="first">id físico: </td><td class="second"><div class="id">1</div></td></tr>
                   <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@2:1</div></td></tr>
                   <tr><td class="first">versión: </td><td class="second">0.18</td></tr>
                   <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                   <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb"><tr><td class="sub-first"> driver</td><td>=</td><td>hub</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>100mA</td></tr><tr><td class="sub-first"> slots</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                </table></div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:0">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:0</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">descripción: </td><td class="second">Vídeo</td></tr>
                      <tr><td class="first">producto: </td><td class="second">HP Webcam</td></tr>
                      <tr><td class="first">fabricante: </td><td class="second">Chicony Electronics Co.,Ltd.</td></tr>
                      <tr><td class="first">id físico: </td><td class="second"><div class="id">2</div></td></tr>
                      <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@2:1.2</div></td></tr>
                      <tr><td class="first">versión: </td><td class="second">18.06</td></tr>
                      <tr><td class="first">serie: </td><td class="second">0x0001</td></tr>
                      <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> </td></tr>
                      <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:0"><tr><td class="sub-first"> driver</td><td>=</td><td>uvcvideo</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>500mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
                   </div>
<div class="indented">
                                      <div class="indented">
                   <table width="100%" class="node" summary="attributes of usb:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">usb:1</div></td></tr></thead>
 <tbody>
                      <tr><td class="first">descripción: </td><td class="second">Dispositivo de almacenamiento masivo</td></tr>
                      <tr><td class="first">producto: </td><td class="second">USB2.0-CRW</td></tr>
                      <tr><td class="first">fabricante: </td><td class="second">Generic</td></tr>
                      <tr><td class="first">id físico: </td><td class="second"><div class="id">3</div></td></tr>
                      <tr><td class="first">información del bus: </td><td class="second"><div class="id">usb@2:1.3</div></td></tr>
                      <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">scsi2</div></td></tr>
                      <tr><td class="first">versión: </td><td class="second">57.13</td></tr>
                      <tr><td class="first">serie: </td><td class="second">20120926571200000</td></tr>
                      <tr><td class="first">capacidades: </td><td class="second"><dfn title="USB 2.0">usb-2.00</dfn> <dfn title="SCSI">scsi</dfn> <dfn title="Emulated device">emulated</dfn> <dfn title="SCSI host adapter">scsi-host</dfn> </td></tr>
                      <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de usb:1"><tr><td class="sub-first"> driver</td><td>=</td><td>usb-storage</td></tr><tr><td class="sub-first"> maxpower</td><td>=</td><td>500mA</td></tr><tr><td class="sub-first"> speed</td><td>=</td><td>480Mbit/s</td></tr></table></td></tr>
 </tbody>                   </table></div>
<div class="indented">
                                            <div class="indented">
                      <table width="100%" class="node" summary="attributes of disk">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">disk</div></td></tr></thead>
 <tbody>
                         <tr><td class="first">descripción: </td><td class="second">SCSI Disk</td></tr>
                         <tr><td class="first">producto: </td><td class="second">SD/MMC/MS PRO</td></tr>
                         <tr><td class="first">fabricante: </td><td class="second">Generic-</td></tr>
                         <tr><td class="first">id físico: </td><td class="second"><div class="id">0.0.0</div></td></tr>
                         <tr><td class="first">información del bus: </td><td class="second"><div class="id">scsi@2:0.0.0</div></td></tr>
                         <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">/dev/sdb</div></td></tr>
                         <tr><td class="first">versión: </td><td class="second">1.00</td></tr>
                         <tr><td class="first">serie: </td><td class="second">2012062914345300</td></tr>
                         <tr><td class="first">capacidades: </td><td class="second"><dfn title="support is removable">removable</dfn> </td></tr>
                         <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de disk"><tr><td class="sub-first"> ansiversion</td><td>=</td><td>4</td></tr><tr><td class="sub-first"> logicalsectorsize</td><td>=</td><td>512</td></tr><tr><td class="sub-first"> sectorsize</td><td>=</td><td>512</td></tr></table></td></tr>
 </tbody>                      </table></div>
<div class="indented">
                                                  <div class="indented">
                         <table width="100%" class="node" summary="attributes of medium">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">medium</div></td></tr></thead>
 <tbody>
                            <tr><td class="first">id físico: </td><td class="second"><div class="id">0</div></td></tr>
                            <tr><td class="first">nombre lógico: </td><td class="second"><div class="id">/dev/sdb</div></td></tr>
 </tbody>                         </table></div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of serial">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">serial</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">SMBus</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH SMBus Controller</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">14</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:14.0</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">42</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de serial"><tr><td class="sub-first"> driver</td><td>=</td><td>piix4_smbus</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de serial"><tr><td class="sub-first"> irq</td><td>:</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of multimedia:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">multimedia:1</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">Audio device</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH Azalia Controller</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">14.2</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:14.2</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">02</td></tr>
             <tr><td class="first">anchura: </td><td class="second">64 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">33MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="Power Management">pm</dfn> <dfn title="bus mastering">bus_master</dfn> <dfn title="PCI capabilities listing">cap_list</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de multimedia:1"><tr><td class="sub-first"> driver</td><td>=</td><td>snd_hda_intel</td></tr><tr><td class="sub-first"> latency</td><td>=</td><td>32</td></tr></table></td></tr>
             <tr><td class="first">recursos:</td><td class="second"><table summary="recursos de multimedia:1"><tr><td class="sub-first"> irq</td><td>:</td><td>16</td></tr><tr><td class="sub-first"> memoria</td><td>:</td><td>fea60000-fea63fff</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
<div class="indented">
                    <div class="indented">
          <table width="100%" class="node" summary="attributes of isa">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">isa</div></td></tr></thead>
 <tbody>
             <tr><td class="first">descripción: </td><td class="second">ISA bridge</td></tr>
             <tr><td class="first">producto: </td><td class="second">FCH LPC Bridge</td></tr>
             <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
             <tr><td class="first">id físico: </td><td class="second"><div class="id">14.3</div></td></tr>
             <tr><td class="first">información del bus: </td><td class="second"><div class="id">pci@0000:00:14.3</div></td></tr>
             <tr><td class="first">versión: </td><td class="second">11</td></tr>
             <tr><td class="first">anchura: </td><td class="second">32 bits</td></tr>
             <tr><td class="first">reloj: </td><td class="second">66MHz</td></tr>
             <tr><td class="first">capacidades: </td><td class="second"><dfn title="">isa</dfn> <dfn title="bus mastering">bus_master</dfn> </td></tr>
             <tr><td class="first">configuración:</td><td class="second"><table summary="configuración de isa"><tr><td class="sub-first"> latency</td><td>=</td><td>0</td></tr></table></td></tr>
 </tbody>          </table></div>
          </div>
       </div>
<div class="indented">
              <div class="indented">
       <table width="100%" class="node" summary="attributes of pci:1">
 <thead><tr><td class="first">id:</td><td class="second"><div class="id">pci:1</div></td></tr></thead>
 <tbody>
          <tr><td class="first">descripción: </td><td class="second">Host bridge</td></tr>
          <tr><td class="first">producto: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">fabricante: </td><td class="second">Advanced Micro Devices, Inc. [AMD]</td></tr>
          <tr><td class="first">id físico: </td><td class="second"><div class="id">101</div></td></tr>
          <tr><td class="first">info