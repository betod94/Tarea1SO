# lshw -businfo

## -businfo Nos muestra la lista del dispositivo mostrando informacion
## del bus, detallando SCSI, USB, IDE y direcciones PCI.




información del Bus  Dispositivo  Clase          Descripción
==============================================================
                                   system         20-c018la (X5Z67AA#ABM)
                                   bus            8245
                                   memory         64KiB BIOS
                                   memory         4GiB Memoria de sistema
                                   memory         4GiB SODIMM DDR3 Síncrono Unb
                                   memory         256KiB L1 caché
                                   memory         2MiB L2 caché
cpu@0                              processor      AMD A4-7210 APU with AMD Radeo
pci@0000:00:00.0                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:01.0                   display        Mullins [Radeon R3 Graphics]
pci@0000:00:01.1                   multimedia     Kabini HDMI/DP Audio
pci@0000:00:02.2                   bridge         Family 16h Processor Functions
pci@0000:01:00.0      wlp1s0       network        RTL8723BE PCIe Wireless Networ
pci@0000:00:02.3                   bridge         Family 16h Processor Functions
pci@0000:02:00.0      enp2s0       network        RTL8111/8168/8411 PCI Express 
pci@0000:00:08.0                   generic        Advanced Micro Devices, Inc. [
pci@0000:00:10.0                   bus            FCH USB XHCI Controller
usb@3                 usb3         bus            xHCI Host Controller
usb@4                 usb4         bus            xHCI Host Controller
pci@0000:00:11.0                   storage        FCH SATA Controller [AHCI mode
pci@0000:00:12.0                   bus            FCH USB EHCI Controller
usb@1                 usb1         bus            EHCI Host Controller
usb@1:1                            bus            USB hub
usb@1:1.1                          input          USB Optical Mouse
usb@1:1.2                          input          USB Keyboard
usb@1:1.3                          communication  Bluetooth Radio
pci@0000:00:13.0                   bus            FCH USB EHCI Controller
usb@2                 usb2         bus            EHCI Host Controller
usb@2:1                            bus            USB hub
usb@2:1.2                          multimedia     HP Webcam
usb@2:1.3             scsi2        storage        USB2.0-CRW
scsi@2:0.0.0          /dev/sdb     disk           SD/MMC/MS PRO
                      /dev/sdb     disk           
pci@0000:00:14.0                   bus            FCH SMBus Controller
pci@0000:00:14.2                   multimedia     FCH Azalia Controller
pci@0000:00:14.3                   bridge         FCH LPC Bridge
pci@0000:00:02.0                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:18.0                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:18.1                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:18.2                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:18.3                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:18.4                   bridge         Advanced Micro Devices, Inc. [
pci@0000:00:18.5                   bridge         Advanced Micro Devices, Inc. [
                      scsi0        storage        
scsi@0:0.0.0          /dev/sda     disk           500GB TOSHIBA DT01ACA0
scsi@0:0.0.0,1                     volume         259MiB Windows FAT volumen
scsi@0:0.0.0,2        /dev/sda2    volume         15MiB reserved partition
scsi@0:0.0.0,3        /dev/sda3    volume         427GiB Windows NTFS volumen
scsi@0:0.0.0,4        /dev/sda4    volume         979MiB Windows NTFS volumen
scsi@0:0.0.0,5        /dev/sda5    volume         17GiB Windows NTFS volumen
scsi@0:0.0.0,6        /dev/sda6    volume         19GiB partición EXT4
                      scsi1        storage        
scsi@1:0.0.0          /dev/cdrom   disk           DVDRW  GUD1N
                                   power          Standard Efficiency
