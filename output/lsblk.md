# lsblk

NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0   260M  0 part /boot/efi
├─sda2   8:2    0    16M  0 part 
├─sda3   8:3    0 427.8G  0 part 
├─sda4   8:4    0   980M  0 part 
├─sda5   8:5    0  17.2G  0 part 
└─sda6   8:6    0  19.5G  0 part /
sr0     11:0    1  1024M  0 rom

