# lshw -short

## -short Devuelve el arbol del dispositivo mostrando caminos de hardware,
##  muy parecido a lo que devolvería el ioscan de HP-UX

ruta H/W                 Dispositivo  Clase          Descripción
=================================================================
                                      system         20-c018la (X5Z67AA#ABM)
/0                                    bus            8245
/0/0                                  memory         64KiB BIOS
/0/27                                 memory         4GiB Memoria de sistema
/0/27/0                               memory         4GiB SODIMM DDR3 Síncrono 
/0/2b                                 memory         256KiB L1 caché
/0/2c                                 memory         2MiB L2 caché
/0/2d                                 processor      AMD A4-7210 APU with AMD Ra
/0/100                                bridge         Advanced Micro Devices, Inc
/0/100/1                              display        Mullins [Radeon R3 Graphics
/0/100/1.1                            multimedia     Kabini HDMI/DP Audio
/0/100/2.2                            bridge         Family 16h Processor Functi
/0/100/2.2/0             wlp1s0       network        RTL8723BE PCIe Wireless Net
/0/100/2.3                            bridge         Family 16h Processor Functi
/0/100/2.3/0             enp2s0       network        RTL8111/8168/8411 PCI Expre
/0/100/8                              generic        Advanced Micro Devices, Inc
/0/100/10                             bus            FCH USB XHCI Controller
/0/100/10/0              usb3         bus            xHCI Host Controller
/0/100/10/1              usb4         bus            xHCI Host Controller
/0/100/11                             storage        FCH SATA Controller [AHCI m
/0/100/12                             bus            FCH USB EHCI Controller
/0/100/12/1              usb1         bus            EHCI Host Controller
/0/100/12/1/1                         bus            USB hub
/0/100/12/1/1/1                       input          USB Optical Mouse
/0/100/12/1/1/2                       input          USB Keyboard
/0/100/12/1/1/3                       communication  Bluetooth Radio
/0/100/13                             bus            FCH USB EHCI Controller
/0/100/13/1              usb2         bus            EHCI Host Controller
/0/100/13/1/1                         bus            USB hub
/0/100/13/1/1/2                       multimedia     HP Webcam
/0/100/13/1/1/3          scsi2        storage        USB2.0-CRW
/0/100/13/1/1/3/0.0.0    /dev/sdb     disk           SD/MMC/MS PRO
/0/100/13/1/1/3/0.0.0/0  /dev/sdb     disk           
/0/100/14                             bus            FCH SMBus Controller
/0/100/14.2                           multimedia     FCH Azalia Controller
/0/100/14.3                           bridge         FCH LPC Bridge
/0/101                                bridge         Advanced Micro Devices, Inc
/0/102                                bridge         Advanced Micro Devices, Inc
/0/103                                bridge         Advanced Micro Devices, Inc
/0/104                                bridge         Advanced Micro Devices, Inc
/0/105                                bridge         Advanced Micro Devices, Inc
/0/106                                bridge         Advanced Micro Devices, Inc
/0/107                                bridge         Advanced Micro Devices, Inc
/0/1                     scsi0        storage        
/0/1/0.0.0               /dev/sda     disk           500GB TOSHIBA DT01ACA0
/0/1/0.0.0/1                          volume         259MiB Windows FAT volumen
/0/1/0.0.0/2             /dev/sda2    volume         15MiB reserved partition
/0/1/0.0.0/3             /dev/sda3    volume         427GiB Windows NTFS volumen
/0/1/0.0.0/4             /dev/sda4    volume         979MiB Windows NTFS volumen
/0/1/0.0.0/5             /dev/sda5    volume         17GiB Windows NTFS volumen
/0/1/0.0.0/6             /dev/sda6    volume         19GiB partición EXT4
/0/2                     scsi1        storage        
/0/2/0.0.0               /dev/cdrom   disk           DVDRW  GUD1N
/1                                    power          Standard Efficiency
