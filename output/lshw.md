# lshw

centralsystem-20-c018la     
    descripción: Todo en uno
    producto: 20-c018la (X5Z67AA#ABM)
    fabricante: HP
    serie: 8CC6510TF5
    anchura: 64 bits
    capacidades: smbios-2.8 dmi-2.8 smp vsyscall32
    configuración: boot=normal chassis=all-in-one family=103C_53316J HP Desktop sku=X5Z67AA#ABM uuid=7728D23C-AC20-C7E9-BE2D-CEF2AD4EA42B
  *-core
       descripción: Placa base
       producto: 8245
       fabricante: HP
       id físico: 0
       versión: 001
       serie: PGBNT0A8J570N9
     *-firmware
          descripción: BIOS
          fabricante: AMI
          id físico: 0
          versión: F.20
          date: 10/11/2016
          tamaño: 64KiB
          capacidad: 8128KiB
          capacidades: pci pnp upgrade shadowing cdboot bootselect edd int5printscreen int9keyboard int14serial int17printer acpi usb biosbootspecification netboot uefi
     *-memory
          descripción: Memoria de sistema
          id físico: 27
          ranura: Placa de sistema o placa base
          tamaño: 4GiB
        *-bank
             descripción: SODIMM DDR3 Síncrono Unbuffered (Unregistered) 1600 MHz (0.6 ns)
             producto: M471B5173EB0-YK0
             fabricante: Samsung
             id físico: 0
             serie: 1763F034
             ranura: DIMM 0
             tamaño: 4GiB
             anchura: 64 bits
             reloj: 1600MHz (0.6ns)
     *-cache:0
          descripción: L1 caché
          id físico: 2b
          ranura: L1 CACHE
          tamaño: 256KiB
          capacidad: 256KiB
          reloj: 1GHz (1.0ns)
          capacidades: pipeline-burst internal write-back unified
          configuración: level=1
     *-cache:1
          descripción: L2 caché
          id físico: 2c
          ranura: L2 CACHE
          tamaño: 2MiB
          capacidad: 2MiB
          reloj: 1GHz (1.0ns)
          capacidades: pipeline-burst internal write-back unified
          configuración: level=2
     *-cpu
          descripción: CPU
          producto: AMD A4-7210 APU with AMD Radeon R3 Graphics
          fabricante: Advanced Micro Devices [AMD]
          id físico: 2d
          información del bus: cpu@0
          versión: AMD A4-7210 APU with AMD Radeon R3 Graphics
          ranura: P0
          tamaño: 1334MHz
          capacidad: 1800MHz
          anchura: 64 bits
          reloj: 100MHz
          capacidades: x86-64 fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp constant_tsc rep_good acc_power nopl nonstop_tsc cpuid extd_apicid aperfmperf pni pclmulqdq monitor ssse3 cx16 sse4_1 sse4_2 movbe popcnt aes xsave avx f16c rdrand lahf_lm cmp_legacy svm extapic cr8_legacy abm sse4a misalignsse 3dnowprefetch osvw ibs skinit wdt topoext perfctr_nb bpext ptsc perfctr_l2 cpb hw_pstate retpoline retpoline_amd rsb_ctxsw ssbd ls_cfg_ssbd vmmcall bmi1 xsaveopt arat npt lbrv svm_lock nrip_save tsc_scale flushbyasid decodeassists pausefilter pfthreshold overflow_recov cpufreq
          configuración: cores=4 enabledcores=4 threads=4
     *-pci:0
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 100
          información del bus: pci@0000:00:00.0
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
        *-display
             descripción: VGA compatible controller
             producto: Mullins [Radeon R3 Graphics]
             fabricante: Advanced Micro Devices, Inc. [AMD/ATI]
             id físico: 1
             información del bus: pci@0000:00:01.0
             versión: 45
             anchura: 64 bits
             reloj: 33MHz
             capacidades: pm pciexpress msi vga_controller bus_master cap_list rom
             configuración: driver=radeon latency=0
             recursos: irq:37 memoria:c0000000-cfffffff memoria:d0000000-d07fffff ioport:f000(size=256) memoria:fea00000-fea3ffff memoria:c0000-dffff
        *-multimedia:0
             descripción: Audio device
             producto: Kabini HDMI/DP Audio
             fabricante: Advanced Micro Devices, Inc. [AMD/ATI]
             id físico: 1.1
             información del bus: pci@0000:00:01.1
             versión: 00
             anchura: 64 bits
             reloj: 33MHz
             capacidades: pm pciexpress msi bus_master cap_list
             configuración: driver=snd_hda_intel latency=0
             recursos: irq:42 memoria:fea64000-fea67fff
        *-pci:0
             descripción: PCI bridge
             producto: Family 16h Processor Functions 5:1
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 2.2
             información del bus: pci@0000:00:02.2
             versión: 00
             anchura: 32 bits
             reloj: 33MHz
             capacidades: pci pm pciexpress msi ht normal_decode bus_master cap_list
             configuración: driver=pcieport
             recursos: irq:25 ioport:e000(size=4096) memoria:fe900000-fe9fffff
           *-network
                descripción: Interfaz inalámbrica
                producto: RTL8723BE PCIe Wireless Network Adapter
                fabricante: Realtek Semiconductor Co., Ltd.
                id físico: 0
                información del bus: pci@0000:01:00.0
                nombre lógico: wlp1s0
                versión: 00
                serie: 3c:a0:67:68:1a:5f
                anchura: 64 bits
                reloj: 33MHz
                capacidades: pm msi pciexpress bus_master cap_list ethernet physical wireless
                configuración: broadcast=yes driver=rtl8723be driverversion=4.13.0-46-generic firmware=N/A ip=192.168.1.91 latency=0 link=yes multicast=yes wireless=IEEE 802.11
                recursos: irq:40 ioport:e000(size=256) memoria:fe900000-fe903fff
        *-pci:1
             descripción: PCI bridge
             producto: Family 16h Processor Functions 5:1
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 2.3
             información del bus: pci@0000:00:02.3
             versión: 00
             anchura: 32 bits
             reloj: 33MHz
             capacidades: pci pm pciexpress msi ht normal_decode bus_master cap_list
             configuración: driver=pcieport
             recursos: irq:27 ioport:d000(size=4096) memoria:fe800000-fe8fffff
           *-network
                descripción: Ethernet interface
                producto: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
                fabricante: Realtek Semiconductor Co., Ltd.
                id físico: 0
                información del bus: pci@0000:02:00.0
                nombre lógico: enp2s0
                versión: 10
                serie: c8:d3:ff:e0:e8:ca
                tamaño: 10Mbit/s
                capacidad: 1Gbit/s
                anchura: 64 bits
                reloj: 33MHz
                capacidades: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd 1000bt 1000bt-fd autonegotiation
                configuración: autonegotiation=on broadcast=yes driver=r8169 driverversion=2.3LK-NAPI duplex=half firmware=rtl8168g-3_0.0.1 04/23/13 latency=0 link=no multicast=yes port=MII speed=10Mbit/s
                recursos: irq:35 ioport:d000(size=256) memoria:fe804000-fe804fff memoria:fe800000-fe803fff
        *-generic
             descripción: Encryption controller
             producto: Advanced Micro Devices, Inc. [AMD]
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 8
             información del bus: pci@0000:00:08.0
             versión: 00
             anchura: 64 bits
             reloj: 33MHz
             capacidades: msix ht pm bus_master cap_list
             configuración: driver=ccp latency=0
             recursos: irq:0 memoria:d0800000-d081ffff memoria:fe700000-fe7fffff memoria:fea6f000-fea6ffff memoria:fea6a000-fea6bfff
        *-usb:0
             descripción: USB controller
             producto: FCH USB XHCI Controller
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 10
             información del bus: pci@0000:00:10.0
             versión: 11
             anchura: 64 bits
             reloj: 33MHz
             capacidades: pm msi msix pciexpress xhci bus_master cap_list
             configuración: driver=xhci_hcd latency=0
             recursos: irq:18 memoria:fea68000-fea69fff
           *-usbhost:0
                producto: xHCI Host Controller
                fabricante: Linux 4.13.0-46-generic xhci-hcd
                id físico: 0
                información del bus: usb@3
                nombre lógico: usb3
                versión: 4.13
                capacidades: usb-2.00
                configuración: driver=hub slots=2 speed=480Mbit/s
           *-usbhost:1
                producto: xHCI Host Controller
                fabricante: Linux 4.13.0-46-generic xhci-hcd
                id físico: 1
                información del bus: usb@4
                nombre lógico: usb4
                versión: 4.13
                capacidades: usb-3.00
                configuración: driver=hub slots=2 speed=5000Mbit/s
        *-storage
             descripción: SATA controller
             producto: FCH SATA Controller [AHCI mode]
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 11
             información del bus: pci@0000:00:11.0
             versión: 39
             anchura: 32 bits
             reloj: 66MHz
             capacidades: storage pm msi ahci_1.0 bus_master cap_list
             configuración: driver=ahci latency=32
             recursos: irq:34 ioport:f140(size=8) ioport:f130(size=4) ioport:f120(size=8) ioport:f110(size=4) ioport:f100(size=16) memoria:fea6e000-fea6e3ff
        *-usb:1
             descripción: USB controller
             producto: FCH USB EHCI Controller
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 12
             información del bus: pci@0000:00:12.0
             versión: 39
             anchura: 32 bits
             reloj: 66MHz
             capacidades: pm debug ehci bus_master cap_list
             configuración: driver=ehci-pci latency=32
             recursos: irq:18 memoria:fea6d000-fea6d0ff
           *-usbhost
                producto: EHCI Host Controller
                fabricante: Linux 4.13.0-46-generic ehci_hcd
                id físico: 1
                información del bus: usb@1
                nombre lógico: usb1
                versión: 4.13
                capacidades: usb-2.00
                configuración: driver=hub slots=2 speed=480Mbit/s
              *-usb
                   descripción: USB hub
                   fabricante: Advanced Micro Devices, Inc.
                   id físico: 1
                   información del bus: usb@1:1
                   versión: 0.18
                   capacidades: usb-2.00
                   configuración: driver=hub maxpower=100mA slots=4 speed=480Mbit/s
                 *-usb:0
                      descripción: Ratón
                      producto: USB Optical Mouse
                      fabricante: PixArt
                      id físico: 1
                      información del bus: usb@1:1.1
                      versión: 1.00
                      capacidades: usb-2.00
                      configuración: driver=usbhid maxpower=100mA speed=2Mbit/s
                 *-usb:1
                      descripción: Teclado
                      producto: USB Keyboard
                      fabricante: NOVATEK
                      id físico: 2
                      información del bus: usb@1:1.2
                      versión: 1.04
                      capacidades: usb-1.10
                      configuración: driver=usbhid maxpower=100mA speed=2Mbit/s
                 *-usb:2
                      descripción: Interfaz Bluetooth
                      producto: Bluetooth Radio
                      fabricante: Realtek
                      id físico: 3
                      información del bus: usb@1:1.3
                      versión: 2.00
                      serie: 00e04c000001
                      capacidades: bluetooth usb-2.10
                      configuración: driver=btusb maxpower=500mA speed=12Mbit/s
        *-usb:2
             descripción: USB controller
             producto: FCH USB EHCI Controller
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 13
             información del bus: pci@0000:00:13.0
             versión: 39
             anchura: 32 bits
             reloj: 66MHz
             capacidades: pm debug ehci bus_master cap_list
             configuración: driver=ehci-pci latency=32
             recursos: irq:18 memoria:fea6c000-fea6c0ff
           *-usbhost
                producto: EHCI Host Controller
                fabricante: Linux 4.13.0-46-generic ehci_hcd
                id físico: 1
                información del bus: usb@2
                nombre lógico: usb2
                versión: 4.13
                capacidades: usb-2.00
                configuración: driver=hub slots=2 speed=480Mbit/s
              *-usb
                   descripción: USB hub
                   fabricante: Advanced Micro Devices, Inc.
                   id físico: 1
                   información del bus: usb@2:1
                   versión: 0.18
                   capacidades: usb-2.00
                   configuración: driver=hub maxpower=100mA slots=4 speed=480Mbit/s
                 *-usb:0
                      descripción: Vídeo
                      producto: HP Webcam
                      fabricante: Chicony Electronics Co.,Ltd.
                      id físico: 2
                      información del bus: usb@2:1.2
                      versión: 18.06
                      serie: 0x0001
                      capacidades: usb-2.00
                      configuración: driver=uvcvideo maxpower=500mA speed=480Mbit/s
                 *-usb:1
                      descripción: Dispositivo de almacenamiento masivo
                      producto: USB2.0-CRW
                      fabricante: Generic
                      id físico: 3
                      información del bus: usb@2:1.3
                      nombre lógico: scsi2
                      versión: 57.13
                      serie: 20120926571200000
                      capacidades: usb-2.00 scsi emulated scsi-host
                      configuración: driver=usb-storage maxpower=500mA speed=480Mbit/s
                    *-disk
                         descripción: SCSI Disk
                         producto: SD/MMC/MS PRO
                         fabricante: Generic-
                         id físico: 0.0.0
                         información del bus: scsi@2:0.0.0
                         nombre lógico: /dev/sdb
                         versión: 1.00
                         serie: 2012062914345300
                         capacidades: removable
                         configuración: ansiversion=4 logicalsectorsize=512 sectorsize=512
                       *-medium
                            id físico: 0
                            nombre lógico: /dev/sdb
        *-serial
             descripción: SMBus
             producto: FCH SMBus Controller
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 14
             información del bus: pci@0000:00:14.0
             versión: 42
             anchura: 32 bits
             reloj: 66MHz
             configuración: driver=piix4_smbus latency=0
             recursos: irq:0
        *-multimedia:1
             descripción: Audio device
             producto: FCH Azalia Controller
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 14.2
             información del bus: pci@0000:00:14.2
             versión: 02
             anchura: 64 bits
             reloj: 33MHz
             capacidades: pm bus_master cap_list
             configuración: driver=snd_hda_intel latency=32
             recursos: irq:16 memoria:fea60000-fea63fff
        *-isa
             descripción: ISA bridge
             producto: FCH LPC Bridge
             fabricante: Advanced Micro Devices, Inc. [AMD]
             id físico: 14.3
             información del bus: pci@0000:00:14.3
             versión: 11
             anchura: 32 bits
             reloj: 66MHz
             capacidades: isa bus_master
             configuración: latency=0
     *-pci:1
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 101
          información del bus: pci@0000:00:02.0
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
     *-pci:2
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 102
          información del bus: pci@0000:00:18.0
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
     *-pci:3
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 103
          información del bus: pci@0000:00:18.1
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
     *-pci:4
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 104
          información del bus: pci@0000:00:18.2
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
     *-pci:5
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 105
          información del bus: pci@0000:00:18.3
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
          configuración: driver=k10temp
          recursos: irq:0
     *-pci:6
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 106
          información del bus: pci@0000:00:18.4
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
          configuración: driver=fam15h_power
          recursos: irq:0
     *-pci:7
          descripción: Host bridge
          producto: Advanced Micro Devices, Inc. [AMD]
          fabricante: Advanced Micro Devices, Inc. [AMD]
          id físico: 107
          información del bus: pci@0000:00:18.5
          versión: 00
          anchura: 32 bits
          reloj: 33MHz
     *-scsi:0
          id físico: 1
          nombre lógico: scsi0
          capacidades: emulated
        *-disk
             descripción: ATA Disk
             producto: TOSHIBA DT01ACA0
             fabricante: Toshiba
             id físico: 0.0.0
             información del bus: scsi@0:0.0.0
             nombre lógico: /dev/sda
             versión: A800
             serie: Y6HWY9HAS
             tamaño: 465GiB (500GB)
             capacidades: gpt-1.00 partitioned partitioned:gpt
             configuración: ansiversion=5 guid=396da9a9-0aa8-4927-b235-e8b5818f2bea logicalsectorsize=512 sectorsize=4096
           *-volume:0 NO RECLAMADO
                descripción: Windows FAT volumen
                fabricante: MSDOS5.0
                id físico: 1
                información del bus: scsi@0:0.0.0,1
                versión: FAT32
                serie: a64e-cd3d
                tamaño: 255MiB
                capacidad: 259MiB
                capacidades: boot fat initialized
                configuración: FATs=2 filesystem=fat name=EFI system partition
           *-volume:1
                descripción: reserved partition
                fabricante: Windows
                id físico: 2
                información del bus: scsi@0:0.0.0,2
                nombre lógico: /dev/sda2
                serie: 9a1226ac-8f27-4cea-9d90-220eb61f35b2
                capacidad: 15MiB
                capacidades: nofs
                configuración: name=Microsoft reserved partition
           *-volume:2
                descripción: Windows NTFS volumen
                fabricante: Windows
                id físico: 3
                información del bus: scsi@0:0.0.0,3
                nombre lógico: /dev/sda3
                versión: 3.1
                serie: 76ea71d5-a01d-8441-a022-03bf4123cd43
                tamaño: 427GiB
                capacidad: 427GiB
                capacidades: ntfs initialized
                configuración: clustersize=4096 created=2016-09-09 22:12:46 filesystem=ntfs label=Windows name=Basic data partition state=clean
           *-volume:3
                descripción: Windows NTFS volumen
                fabricante: Windows
                id físico: 4
                información del bus: scsi@0:0.0.0,4
                nombre lógico: /dev/sda4
                versión: 3.1
                serie: c074-82f3
                tamaño: 957MiB
                capacidad: 979MiB
                capacidades: boot precious readonly hidden nomount ntfs initialized
                configuración: clustersize=4096 created=2016-09-10 11:43:27 filesystem=ntfs label=Windows RE tools modified_by_chkdsk=true mounted_on_nt4=true name=Basic data partition resize_log_file=true state=dirty upgrade_on_mount=true
           *-volume:4
                descripción: Windows NTFS volumen
                fabricante: Windows
                id físico: 5
                información del bus: scsi@0:0.0.0,5
                nombre lógico: /dev/sda5
                versión: 3.1
                serie: 42b7a10b-e7d2-6549-8246-ab66268bfdc4
                tamaño: 17GiB
                capacidad: 17GiB
                capacidades: precious readonly hidden nomount ntfs initialized
                configuración: clustersize=4096 created=2016-09-10 11:36:29 filesystem=ntfs label=RECOVERY name=Basic data partition state=clean
           *-volume:5
                descripción: partición EXT4
                fabricante: Linux
                id físico: 6
                información del bus: scsi@0:0.0.0,6
                nombre lógico: /dev/sda6
                nombre lógico: /
                versión: 1.0
                serie: 3f7d8727-c791-4e97-b742-c661df9bcff5
                tamaño: 19GiB
                capacidades: journaled extended_attributes large_files huge_files dir_nlink recover 64bit extents ext4 ext2 initialized
                configuración: created=2017-04-17 01:49:02 filesystem=ext4 lastmountpoint=/ modified=2018-08-22 16:32:35 mount.fstype=ext4 mount.options=rw,relatime,errors=remount-ro,data=ordered mounted=2018-08-22 16:32:48 state=mounted
     *-scsi:1
          id físico: 2
          nombre lógico: scsi1
          capacidades: emulated
        *-cdrom
             descripción: DVD-RAM writer
             producto: DVDRW  GUD1N
             fabricante: hp HLDS
             id físico: 0.0.0
             información del bus: scsi@1:0.0.0
             nombre lógico: /dev/cdrom
             nombre lógico: /dev/cdrw
             nombre lógico: /dev/dvd
             nombre lógico: /dev/dvdrw
             nombre lógico: /dev/sr0
             versión: LD02
             capacidades: removable audio cd-r cd-rw dvd dvd-r dvd-ram
             configuración: ansiversion=5 status=nodisc
  *-power NO RECLAMADO
       producto: Standard Efficiency
       id físico: 1
       capacidad: 32768mWh

