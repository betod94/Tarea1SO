
#smartctl --xall /dev/sda

=== START OF READ SMART DATA SECTION ===
SMART overall-health self-assessment test result: PASSED


SMART Attributes Data Structure revision number: 16
Vendor Specific SMART Attributes with Thresholds:
ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
  1 Raw_Read_Error_Rate     POSR-K   100   082   016    -    0
  2 Throughput_Performance  POS--K   141   100   054    -    72
  3 Spin_Up_Time            PO---K   131   100   024    -    167 (Average 183)
  4 Start_Stop_Count        -O--CK   100   100   000    -    681
  5 Reallocated_Sector_Ct   PO--CK   100   100   005    -    0
  7 Seek_Error_Rate         POSR-K   100   100   067    -    0
  8 Seek_Time_Performance   P-S--K   113   100   020    -    35
  9 Power_On_Hours          -O--CK   100   100   000    -    2283
 10 Spin_Retry_Count        PO--CK   100   100   060    -    0
 12 Power_Cycle_Count       -O--CK   100   100   000    -    681
183 Runtime_Bad_Block       -O--CK   100   100   000    -    0
184 End-to-End_Error        PO--CK   100   100   097    -    0
185 Unknown_Attribute       -O--CK   078   078   000    -    1507327
187 Reported_Uncorrect      -O--CK   100   100   000    -    262144
188 Command_Timeout         -O--CK   100   100   000    -    0
189 High_Fly_Writes         -O-RCK   100   100   000    -    0
190 Airflow_Temperature_Cel -O---K   063   052   000    -    37 (Min/Max 24/37)
192 Power-Off_Retract_Count -O--CK   100   100   000    -    683
193 Load_Cycle_Count        -O--CK   100   100   000    -    683
194 Temperature_Celsius     -O---K   162   127   000    -    37 (Min/Max 17/48)
196 Reallocated_Event_Count -O--CK   100   100   000    -    0
197 Current_Pending_Sector  -O--CK   100   100   000    -    0
198 Offline_Uncorrectable   ----CK   100   100   000    -    0
199 UDMA_CRC_Error_Count    -O--CK   200   200   000    -    0
                            ||||||_ K auto-keep
                            |||||__ C event count
                            ||||___ R error rate
                            |||____ S speed/performance
                            ||_____ O updated online
                            |______ P prefailure warning

General Purpose Log Directory Version 1
SMART           Log Directory Version 1 [multi-sector log support]
Address    Access  R/W   Size  Description
0x00       GPL,SL  R/O      1  Log Directory
0x01           SL  R/O      1  Summary SMART error log
0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x04       GPL     R/O      7  Device Statistics log
0x06           SL  R/O      1  SMART self-test log
0x07       GPL     R/O      1  Extended self-test log
0x09           SL  R/W      1  Selective self-test log
0x10       GPL     R/O      1  SATA NCQ Queued Error log
0x11       GPL     R/O      1  SATA Phy Event Counters log
0x20       GPL     R/O      1  Streaming performance log [OBS-8]
0x21       GPL     R/O      1  Write stream error log
0x22       GPL     R/O      1  Read stream error log
0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe1       GPL,SL  R/W      1  SCT Data Transfer

SMART Extended Comprehensive Error Log Version: 1 (1 sectors)
No Errors Logged

SMART Extended Self-test Log Version: 1 (1 sectors)
Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
# 1  Short offline       Completed without error       00%       816         -

SMART Selective self-test log data structure revision number 1
 SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
    1        0        0  Not_testing
    2        0        0  Not_testing
    3        0        0  Not_testing
    4        0        0  Not_testing
    5        0        0  Not_testing
Selective self-test flags (0x0):
  After scanning selected spans, do NOT read-scan remainder of disk.
If Selective self-test is pending on power-up, resume after 0 minute delay.

SCT Status Version:                  3
SCT Version (vendor specific):       256 (0x0100)
SCT Support Level:                   1
Device State:                        SMART Off-line Data Collection executing in background (4)
Current Temperature:                    37 Celsius
Power Cycle Min/Max Temperature:     24/37 Celsius
Lifetime    Min/Max Temperature:     17/48 Celsius
Under/Over Temperature Limit Count:   0/0

SCT Temperature History Version:     2
Temperature Sampling Period:         1 minute
Temperature Logging Interval:        1 minute
Min/Max recommended Temperature:      0/60 Celsius
Min/Max Temperature Limit:           -40/70 Celsius
Temperature History Size (Index):    128 (114)

Index    Estimated Time   Temperature Celsius
 115    2018-08-20 22:03    38  *******************
 116    2018-08-20 22:04    39  ********************
 117    2018-08-20 22:05    39  ********************
 118    2018-08-20 22:06    38  *******************
 ...    ..(  3 skipped).    ..  *******************
 122    2018-08-20 22:10    38  *******************
 123    2018-08-20 22:11    39  ********************
 124    2018-08-20 22:12    38  *******************
 125    2018-08-20 22:13    39  ********************
 126    2018-08-20 22:14    38  *******************
 ...    ..(  5 skipped).    ..  *******************
   4    2018-08-20 22:20    38  *******************
   5    2018-08-20 22:21    39  ********************
   6    2018-08-20 22:22    38  *******************
 ...    ..(  4 skipped).    ..  *******************
  11    2018-08-20 22:27    38  *******************
  12    2018-08-20 22:28    39  ********************
 ...    ..( 14 skipped).    ..  ********************
  27    2018-08-20 22:43    39  ********************
  28    2018-08-20 22:44    38  *******************
  29    2018-08-20 22:45    39  ********************
 ...    ..( 25 skipped).    ..  ********************
  55    2018-08-20 23:11    39  ********************
  56    2018-08-20 23:12    38  *******************
  57    2018-08-20 23:13    39  ********************
 ...    ..(  2 skipped).    ..  ********************
  60    2018-08-20 23:16    39  ********************
  61    2018-08-20 23:17     ?  -
  62    2018-08-20 23:18    25  ******
  63    2018-08-20 23:19    26  *******
  64    2018-08-20 23:20    26  *******
  65    2018-08-20 23:21    27  ********
  66    2018-08-20 23:22    27  ********
  67    2018-08-20 23:23    28  *********
  68    2018-08-20 23:24    28  *********
  69    2018-08-20 23:25    29  **********
  70    2018-08-20 23:26    29  **********
  71    2018-08-20 23:27    30  ***********
 ...    ..(  2 skipped).    ..  ***********
  74    2018-08-20 23:30    30  ***********
  75    2018-08-20 23:31    31  ************
  76    2018-08-20 23:32    31  ************
  77    2018-08-20 23:33    31  ************
  78    2018-08-20 23:34    32  *************
  79    2018-08-20 23:35    32  *************
  80    2018-08-20 23:36    32  *************
  81    2018-08-20 23:37    33  **************
 ...    ..(  3 skipped).    ..  **************
  85    2018-08-20 23:41    33  **************
  86    2018-08-20 23:42    34  ***************
 ...    ..(  3 skipped).    ..  ***************
  90    2018-08-20 23:46    34  ***************
  91    2018-08-20 23:47    35  ****************
 ...    ..(  6 skipped).    ..  ****************
  98    2018-08-20 23:54    35  ****************
  99    2018-08-20 23:55    36  *****************
 ...    ..(  5 skipped).    ..  *****************
 105    2018-08-21 00:01    36  *****************
 106    2018-08-21 00:02    37  ******************
 ...    ..(  7 skipped).    ..  ******************
 114    2018-08-21 00:10    37  ******************

SCT Error Recovery Control:
           Read:     85 (8.5 seconds)
          Write:     85 (8.5 seconds)

Device Statistics (GP Log 0x04)
Page  Offset Size        Value Flags Description
0x01  =====  =               =  ===  == General Statistics (rev 1) ==
0x01  0x008  4             681  ---  Lifetime Power-On Resets
0x01  0x010  4            2283  ---  Power-on Hours
0x01  0x018  6      9910644364  ---  Logical Sectors Written
0x01  0x020  6       169015243  ---  Number of Write Commands
0x01  0x028  6     35363827053  ---  Logical Sectors Read
0x01  0x030  6       339618421  ---  Number of Read Commands
0x03  =====  =               =  ===  == Rotating Media Statistics (rev 1) ==
0x03  0x008  4            2282  ---  Spindle Motor Power-on Hours
0x03  0x010  4            2282  ---  Head Flying Hours
0x03  0x018  4             683  ---  Head Load Events
0x03  0x020  4               0  ---  Number of Reallocated Logical Sectors
0x03  0x028  4             169  ---  Read Recovery Attempts
0x03  0x030  4               2  ---  Number of Mechanical Start Failures
0x04  =====  =               =  ===  == General Errors Statistics (rev 1) ==
0x04  0x008  4               0  ---  Number of Reported Uncorrectable Errors
0x04  0x010  4               0  ---  Resets Between Cmd Acceptance and Completion
0x05  =====  =               =  ===  == Temperature Statistics (rev 1) ==
0x05  0x008  1              37  ---  Current Temperature
0x05  0x010  1              36  N--  Average Short Term Temperature
0x05  0x018  1              38  N--  Average Long Term Temperature
0x05  0x020  1              48  ---  Highest Temperature
0x05  0x028  1              17  ---  Lowest Temperature
0x05  0x030  1              43  N--  Highest Average Short Term Temperature
0x05  0x038  1              23  N--  Lowest Average Short Term Temperature
0x05  0x040  1              41  N--  Highest Average Long Term Temperature
0x05  0x048  1              25  N--  Lowest Average Long Term Temperature
0x05  0x050  4               0  ---  Time in Over-Temperature
0x05  0x058  1              60  ---  Specified Maximum Operating Temperature
0x05  0x060  4               0  ---  Time in Under-Temperature
0x05  0x068  1               0  ---  Specified Minimum Operating Temperature
0x06  =====  =               =  ===  == Transport Statistics (rev 1) ==
0x06  0x008  4            7016  ---  Number of Hardware Resets
0x06  0x010  4            3292  ---  Number of ASR Events
0x06  0x018  4               0  ---  Number of Interface CRC Errors
                                |||_ C monitored condition met
                                ||__ D supports DSN
                                |___ N normalized value

SATA Phy Event Counters (GP Log 0x11)
ID      Size     Value  Description
0x0001  2            0  Command failed due to ICRC error
0x0002  2            0  R_ERR response for data FIS
0x0003  2            0  R_ERR response for device-to-host data FIS
0x0004  2            0  R_ERR response for host-to-device data FIS
0x0005  2            0  R_ERR response for non-data FIS
0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0009  2            5  Transition from drive PhyRdy to drive PhyNRdy
0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x000b  2            0  CRC errors within host-to-device FIS
0x000d  2            0  Non-CRC errors within host-to-device FIS

centralsystem@centralsystem-20-c018la:~$ SMART Attributes Data Structure revision number: 16
SMART: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Vendor Specific SMART Attributes with Thresholds:
Vendor: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ ID# ATTRIBUTE_NAME          FLAGS    VALUE WORST THRESH FAIL RAW_VALUE
ID#: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   1 Raw_Read_Error_Rate     POSR-K   100   082   016    -    0
1: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   2 Throughput_Performance  POS--K   141   100   054    -    72
2: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   3 Spin_Up_Time            PO---K   131   100   024    -    167 (Average 183)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   4 Start_Stop_Count        -O--CK   100   100   000    -    681
4: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   5 Reallocated_Sector_Ct   PO--CK   100   100   005    -    0
5: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   7 Seek_Error_Rate         POSR-K   100   100   067    -    0
7: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   8 Seek_Time_Performance   P-S--K   113   100   020    -    35
8: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   9 Power_On_Hours          -O--CK   100   100   000    -    2283
9: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  10 Spin_Retry_Count        PO--CK   100   100   060    -    0
10: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  12 Power_Cycle_Count       -O--CK   100   100   000    -    681
12: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 183 Runtime_Bad_Block       -O--CK   100   100   000    -    0
183: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 184 End-to-End_Error        PO--CK   100   100   097    -    0
184: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 185 Unknown_Attribute       -O--CK   078   078   000    -    1507327
185: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 187 Reported_Uncorrect      -O--CK   100   100   000    -    262144
187: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 188 Command_Timeout         -O--CK   100   100   000    -    0
No se ha encontrado la orden «188», quizás quiso decir:
 La orden «z88» del paquete «z88» (universe)
188: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 189 High_Fly_Writes         -O-RCK   100   100   000    -    0
No se ha encontrado la orden «189», quizás quiso decir:
 La orden «c89» del paquete «clang» (universe)
 La orden «c89» del paquete «gcc» (main)
189: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 190 Airflow_Temperature_Cel -O---K   063   052   000    -    37 (Min/Max 24/37)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 192 Power-Off_Retract_Count -O--CK   100   100   000    -    683
192: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 193 Load_Cycle_Count        -O--CK   100   100   000    -    683
193: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 194 Temperature_Celsius     -O---K   162   127   000    -    37 (Min/Max 17/48)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 196 Reallocated_Event_Count -O--CK   100   100   000    -    0
196: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 197 Current_Pending_Sector  -O--CK   100   100   000    -    0
197: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 198 Offline_Uncorrectable   ----CK   100   100   000    -    0
198: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 199 UDMA_CRC_Error_Count    -O--CK   200   200   000    -    0
No se ha encontrado la orden «199», quizás quiso decir:
 La orden «c99» del paquete «clang» (universe)
 La orden «c99» del paquete «gcc» (main)
199: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$                             ||||||_ K auto-keep
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                             |||||__ C event count
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                             ||||___ R error rate
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                             |||____ S speed/performance
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                             ||_____ O updated online
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                             |______ P prefailure warning
bash: error sintáctico cerca del elemento inesperado `|'
centralsystem@centralsystem-20-c018la:~$ 
2centralsystem@centralsystem-20-c018la:~$ General Purpose Log Directory Version General: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ SMART           Log Directory Version 1 [multi-sector log support]
SMART: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Address    Access  R/W   Size  Description
Address: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x00       GPL,SL  R/O      1  Log Directory
0x00: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01           SL  R/O      1  Summary SMART error log
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03       GPL     R/O      1  Ext. Comprehensive SMART error log
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x04       GPL     R/O      7  Device Statistics log
0x04: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x06           SL  R/O      1  SMART self-test log
0x06: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x07       GPL     R/O      1  Extended self-test log
0x07: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x09           SL  R/W      1  Selective self-test log
0x09: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x10       GPL     R/O      1  SATA NCQ Queued Error log
0x10: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x11       GPL     R/O      1  SATA Phy Event Counters log
0x11: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x20       GPL     R/O      1  Streaming performance log [OBS-8]
0x20: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x21       GPL     R/O      1  Write stream error log
0x21: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x22       GPL     R/O      1  Read stream error log
0x22: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x80-0x9f  GPL,SL  R/W     16  Host vendor specific log
0x80-0x9f: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0xe0       GPL,SL  R/W      1  SCT Command/Status
0xe0: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0xe1       GPL,SL  R/W      1  SCT Data Transfer
0xe1: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 
 centralsystem@centralsystem-20-c018la:~$ SMART Extended Comprehensive Error LogVersion: 1 (1 sectors)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ No Errors Logged
No: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 
*centralsystem@centralsystem-20-c018la:~$ SMART Extended Self-test Log Version:  (1 sectors)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ Num  Test_Description    Status                  Remaining  LifeTime(hours)  LBA_of_first_error
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ # 1  Short offline       Completed without error       00%       816         -
centralsystem@centralsystem-20-c018la:~$ 
7centralsystem@centralsystem-20-c018la:~$ SMART Selective self-test log data strcture revision number 1
SMART: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  SPAN  MIN_LBA  MAX_LBA  CURRENT_TEST_STATUS
**

SCT Error Recovery SPAN: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$     1        0        0  Not_testing
1: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$     2        0        0  Not_testing
2: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$     3        0        0  Not_testing
3: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$     4        0        0  Not_testing
4: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$     5        0        0  Not_testing
5: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Selective self-test flags (0x0):
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   After scanning selected spans, do NOT read-scan remainder of disk.
After: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ If Selective self-test is pending on power-up, resume after 0 minute delay.
If: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 
 centralsystem@centralsystem-20-c018la:~$ SCT Status Version:                  3
SCT: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ SCT Version (vendor specific):       256 (0x0100)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ SCT Support Level:                   1
SCT: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Device State:                        SMART Off-line Data Collection executing in background (4)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ Current Temperature:                    37 Celsius
Current: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Power Cycle Min/Max Temperature:     24/37 Celsius
Power: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Lifetime    Min/Max Temperature:     17/48 Celsius
Lifetime: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Under/Over Temperature Limit Count:   0/0
bash: Under/Over: No existe el archivo o el directorio
centralsystem@centralsystem-20-c018la:~$ 
gcentralsystem@centralsystem-20-c018la:~$ SCT Temperature History Version:     2
SCT: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Temperature Sampling Period:         1 minute
Temperature: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Temperature Logging Interval:        1 minute
Temperature: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ Min/Max recommended Temperature:      0/60 Celsius
bash: Min/Max: No existe el archivo o el directorio
centralsystem@centralsystem-20-c018la:~$ Min/Max Temperature Limit:           -40/70 Celsius
bash: Min/Max: No existe el archivo o el directorio
centralsystem@centralsystem-20-c018la:~$ Temperature History Size (Index):    128 (114)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 
 centralsystem@centralsystem-20-c018la:~$ Index    Estimated Time   Temperature elsius
Index: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  115    2018-08-20 22:03    38  *******************
115: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  116    2018-08-20 22:04    39  ********************
116: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  117    2018-08-20 22:05    39  ********************
117: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  118    2018-08-20 22:06    38  *******************
118: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  3 skipped).    ..  *******************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$  122    2018-08-20 22:10    38  *******************
122: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  123    2018-08-20 22:11    39  ********************
123: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  124    2018-08-20 22:12    38  *******************
124: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  125    2018-08-20 22:13    39  ********************
125: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  126    2018-08-20 22:14    38  *******************
126: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  5 skipped).    ..  *******************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$    4    2018-08-20 22:20    38  *******************
4: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$    5    2018-08-20 22:21    39  ********************
5: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$    6    2018-08-20 22:22    38  *******************
6: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  4 skipped).    ..  *******************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   11    2018-08-20 22:27    38  *******************
11: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   12    2018-08-20 22:28    39  ********************
12: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..( 14 skipped).    ..  ********************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   27    2018-08-20 22:43    39  ********************
27: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   28    2018-08-20 22:44    38  *******************
28: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   29    2018-08-20 22:45    39  ********************
29: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..( 25 skipped).    ..  ********************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   55    2018-08-20 23:11    39  ********************
55: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   56    2018-08-20 23:12    38  *******************
56: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   57    2018-08-20 23:13    39  ********************
57: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  2 skipped).    ..  ********************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   60    2018-08-20 23:16    39  ********************
60: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   61    2018-08-20 23:17     ?  -
61: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   62    2018-08-20 23:18    25  ******
62: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   63    2018-08-20 23:19    26  *******63: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   64    2018-08-20 23:20    26  *******64: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   65    2018-08-20 23:21    27  ********
65: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   66    2018-08-20 23:22    27  ********
66: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   67    2018-08-20 23:23    28  *********
67: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   68    2018-08-20 23:24    28  *********
68: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   69    2018-08-20 23:25    29  **********
69: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   70    2018-08-20 23:26    29  **********
70: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   71    2018-08-20 23:27    30  ***********
71: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  2 skipped).    ..  ***********
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   74    2018-08-20 23:30    30  ***********
74: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   75    2018-08-20 23:31    31  ************
75: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   76    2018-08-20 23:32    31  ************
76: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   77    2018-08-20 23:33    31  ************
77: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   78    2018-08-20 23:34    32  *************
78: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   79    2018-08-20 23:35    32  *************
79: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   80    2018-08-20 23:36    32  *************
80: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   81    2018-08-20 23:37    33  **************
81: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  3 skipped).    ..  **************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   85    2018-08-20 23:41    33  **************
85: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   86    2018-08-20 23:42    34  ***************
86: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  3 skipped).    ..  ***************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   90    2018-08-20 23:46    34  ***************
90: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   91    2018-08-20 23:47    35  ****************
91: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  6 skipped).    ..  ****************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$   98    2018-08-20 23:54    35  ****************
98: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$   99    2018-08-20 23:55    36  *****************
99: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  5 skipped).    ..  *****************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$  105    2018-08-21 00:01    36  *****************
105: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  106    2018-08-21 00:02    37  ******************
106: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$  ...    ..(  7 skipped).    ..  ******************
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$  114    2018-08-21 00:10    37  ******************
114: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 
centralsystem@centralsystem-20-c018la:~$ SCT Error Recovery Control:
SCT: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$            Read:     85 (8.5 seconds)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$           Write:     85 (8.5 seconds)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 
centralsystem@centralsystem-20-c018la:~$ Device Statistics (GP Log 0x04)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ Page  Offset Size        Value Flags Description
No se ha encontrado la orden «Page», quizás quiso decir:
 La orden «page» del paquete «tcllib» (universe)
 La orden «Pager» del paquete «afterstep» (universe)
 La orden «Paje» del paquete «paje.app» (universe)
Page: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01  =====  =               =  ===  == General Statistics (rev 1) ==
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 0x01  0x008  4             681  ---  Lifetime Power-On Resets
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01  0x010  4            2283  ---  Power-on Hours
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01  0x018  6      9910644364  ---  Logical Sectors Written
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01  0x020  6       169015243  ---  Number of Write Commands
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01  0x028  6     35363827053  ---  Logical Sectors Read
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x01  0x030  6       339618421  ---  Number of Read Commands
0x01: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03  =====  =               =  ===  == Rotating Media Statistics (rev 1) ==
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 0x03  0x008  4            2282  ---  Spindle Motor Power-on Hours
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03  0x010  4            2282  ---  Head Flying Hours
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03  0x018  4             683  ---  Head Load Events
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03  0x020  4               0  ---  Number of Reallocated Logical Sectors
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03  0x028  4             169  ---  Read Recovery Attempts
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x03  0x030  4               2  ---  Number of Mechanical Start Failures
0x03: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x04  =====  =               =  ===  == General Errors Statistics (rev 1) ==
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 0x04  0x008  4               0  ---  Number of Reported Uncorrectable Errors
0x04: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x04  0x010  4               0  ---  Resets Between Cmd Acceptance and Completion
0x04: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  =====  =               =  ===  == Temperature Statistics (rev 1) ==
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 0x05  0x008  1              37  ---  Current Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x010  1              36  N--  Average Short Term Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x018  1              38  N--  Average Long Term Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x020  1              48  ---  Highest Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x028  1              17  ---  Lowest Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x030  1              43  N--  Highest Average Short Term Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x038  1              23  N--  Lowest Average Short Term Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x040  1              41  N--  Highest Average Long Term Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x048  1              25  N--  Lowest Average Long Term Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x050  4               0  ---  Time in Over-Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x058  1              60  ---  Specified Maximum Operating Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x060  4               0  ---  Time in Under-Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x05  0x068  1               0  ---  Specified Minimum Operating Temperature
0x05: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x06  =====  =               =  ===  == Transport Statistics (rev 1) ==
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ 0x06  0x008  4            7016  ---  Number of Hardware Resets
0x06: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x06  0x010  4            3292  ---  Number of ASR Events
0x06: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x06  0x018  4               0  ---  Number of Interface CRC Errors
0x06: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$                                 |||_ C monitored condition met
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                                 ||__ D supports DSN
bash: error sintáctico cerca del elemento inesperado `||'
centralsystem@centralsystem-20-c018la:~$                                 |___ N normalized value
bash: error sintáctico cerca del elemento inesperado `|'
centralsystem@centralsystem-20-c018la:~$ 
centralsystem@centralsystem-20-c018la:~$ SATA Phy Event Counters (GP Log 0x11)
bash: error sintáctico cerca del elemento inesperado `('
centralsystem@centralsystem-20-c018la:~$ ID      Size     Value  Description
ID: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0001  2            0  Command failed due to ICRC error
0x0001: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0002  2            0  R_ERR response for data FIS
0x0002: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0003  2            0  R_ERR response for device-to-host data FIS
0x0003: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0004  2            0  R_ERR response for host-to-device data FIS
0x0004: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0005  2            0  R_ERR response for non-data FIS
0x0005: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0006  2            0  R_ERR response for device-to-host non-data FIS
0x0006: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0007  2            0  R_ERR response for host-to-device non-data FIS
0x0007: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x0009  2            5  Transition from drive PhyRdy to drive PhyNRdy
0x0009: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x000a  2            1  Device-to-host register FISes sent due to a COMRESET
0x000a: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x000b  2            0  CRC errors within host-to-device FIS
0x000b: no se encontró la orden
centralsystem@centralsystem-20-c018la:~$ 0x000d  2            0  Non-CRC errors within host-to-device FIS
0x000d: no se encontró la orden
