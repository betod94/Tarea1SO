# Interrupts

            CPU0       CPU1       CPU2       CPU3       
   0:         44          0          0          0   IO-APIC    2-edge      timer
   1:          4        182        103        103   IO-APIC    1-edge      i8042
   8:          0          1          0          0   IO-APIC    8-edge      rtc0
   9:          2          1          0          3   IO-APIC    9-fasteoi   acpi
  12:          1          0          1          1   IO-APIC   12-edge      i8042
  16:        204        279        240        289   IO-APIC   16-fasteoi   snd_hda_intel:card1
  18:        109      15991      19693      69234   IO-APIC   18-fasteoi   ehci_hcd:usb1, ehci_hcd:usb2
  25:          0          0          0          0   PCI-MSI 36864-edge      PCIe PME
  27:          0          0          0          0   PCI-MSI 38912-edge      PCIe PME
  28:          0          0          0          0   PCI-MSI 262144-edge      xhci_hcd
  29:          0          0          0          0   PCI-MSI 262145-edge      xhci_hcd
  30:          0          0          0          0   PCI-MSI 262146-edge      xhci_hcd
  31:          0          0          0          0   PCI-MSI 262147-edge      xhci_hcd
  32:          0          0          0          0   PCI-MSI 262148-edge      xhci_hcd
  34:        341      16214      24060      26817   PCI-MSI 278528-edge      ahci[0000:00:11.0]
  35:          0          0          0          0   PCI-MSI 1048576-edge      enp2s0
  37:          1     515019          3          7   PCI-MSI 16384-edge      radeon
  38:          0          0          0          0   PCI-MSI 131072-edge      ccp-1-0
  39:          0          0          0          0   PCI-MSI 131073-edge      ccp-1-1
  40:      53012      69329      93455     238896   IO-APIC    4-fasteoi   rtl_pci
  42:         14         78         88         76   PCI-MSI 18432-edge      snd_hda_intel:card0
 NMI:        109        112        117        121   Non-maskable interrupts
 LOC:    1013998     864494     892826     904896   Local timer interrupts
 SPU:          0          0          0          0   Spurious interrupts
 PMI:        109        112        117        121   Performance monitoring interrupts
 IWI:     531067     477635     475647     489475   IRQ work interrupts
 RTR:          0          0          0          0   APIC ICR read retries
 RES:    3103578    3076016    3046830    3021678   Rescheduling interrupts
 CAL:     250728     234067     266742     263316   Function call interrupts
 TLB:     246697     224982     258693     255384   TLB shootdowns
 TRM:          0          0          0          0   Thermal event interrupts
 THR:          0          0          0          0   Threshold APIC interrupts
 DFR:          0          0          0          0   Deferred Error APIC interrupts
 MCE:          0          0          0          0   Machine check exceptions
 MCP:         37         37         37         37   Machine check polls
 ERR:          0
 MIS:          0
 PIN:          0          0          0          0   Posted-interrupt notification event
 NPI:          0          0          0          0   Nested posted-interrupt event
 PIW:          0          0          0          0   Posted-interrupt wakeup event
 