# Mem info

## Listado del tipo de memoria.

MemTotal:        3451036 kB
MemFree:          296612 kB
MemAvailable:    1507284 kB
Buffers:           56544 kB
Cached:           805780 kB
SwapCached:            0 kB
Active:          1901080 kB
Inactive:         984464 kB
Active(anon):    1282352 kB
Inactive(anon):   217476 kB
Active(file):     618728 kB
Inactive(file):   766988 kB
Unevictable:          16 kB
Mlocked:              16 kB
SwapTotal:        947080 kB
SwapFree:         947080 kB
Dirty:               100 kB
Writeback:             0 kB
AnonPages:       2023268 kB
Mapped:           414460 kB
Shmem:            101452 kB
Slab:             104156 kB
SReclaimable:      55568 kB
SUnreclaim:        48588 kB
KernelStack:       14928 kB
PageTables:        58392 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:     2672596 kB
Committed_AS:    9816520 kB
VmallocTotal:   34359738367 kB
VmallocUsed:           0 kB
VmallocChunk:          0 kB
HardwareCorrupted:     0 kB
AnonHugePages:         0 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
CmaTotal:              0 kB
CmaFree:               0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:      343032 kB
DirectMap2M:     3254272 kB
DirectMap1G:           0 kB
